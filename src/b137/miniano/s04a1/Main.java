package b137.miniano.s04a1;

public class Main {
    public static void main(String[] args) {
        System.out.println("Phonebook Lists\n");

        Contact firstContact = new Contact("Krista Miniano","09171234567","Bangkal Makati");
        firstContact.showDetails();

        System.out.println("__________\n");

        Phonebook addContact = new Phonebook ("Krista Miniano", "09067654321","Trece Martires Cavite");
        addContact.showDetails();

    }

}
