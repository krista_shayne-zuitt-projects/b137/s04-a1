package b137.miniano.s04a1;

public class Contact {
    // Properties

    private String name;
    private String number;
    private String address;

    // Empty Constructor

    public Contact () {

    }

    // Parameterized constructor

    public Contact (String name, String number, String address) {
        this.name = name;
        this.number = number;
        this.address = address;
    }

    // Getters

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getAddress() {
        return address;
    }

    // Setters

    public void setName(String name) {
        this.name = name;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    // Method
    public void showDetails() {
        System.out.println( this.name);
    }

}
