package b137.miniano.s04a1;

public class Phonebook extends Contact {
    // Properties

    private String contactDetails;

    // Empty Constructor
    public Phonebook() { super(); }

    // Parameterized constructor
    public Phonebook (String name, String number, String address) {
        super(name, number, address);
        this.contactDetails = contactDetails;
    }

    // Getters
    public String getContactDetails() {
        return contactDetails;
    }

    // Setters
    public void setContactDetails(String contactDetails) {
        this.contactDetails = contactDetails;
    }

    // Method
    public void phonebook() {
        super.showDetails();
    }

    public void showDetails() {
        System.out.println( super.getName() + " has the following registered number(s):" + super.getNumber());
        System.out.println( super.getName() + " has the following registered addresses:" + super.getAddress());
    }
    
}
